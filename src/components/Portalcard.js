import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function PortalCard({course}) {
const {name, description, price, _id, isActive} = course;

const handleDisable = async (e) => {
    e.preventDefault();
    try {
        const response = await fetch(`/api/courses/${_id}/archive`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({isActive: false})
        });
        
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        const data = await response.json();
        alert(data.message);
    } catch (error) {
        console.log(error);
    }
}

const handleEnable = async (e) => {
    e.preventDefault();
    try {
        const response = await fetch(`/api/courses/${_id}/activate`, {
            method: 'PATCH',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({isActive: true})
        });
        
        if (!response.ok) {
            throw new Error(response.statusText);
        }
        const data = await response.json();
        alert(data.message);
    } catch (error) {
        console.log(error);
    }
}

    

return (
	<div>
		<Card>
			
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP {price}</Card.Text>
				<Button className="bg-primary mx-1" as={Link} to={`/portal/${_id}`} >Details</Button>
                {isActive ? <Button className="bg-danger mx-1" onClick={handleDisable} >Disable</Button> : <Button className="bg-warning mx-1" onClick={handleEnable} >Enable</Button>}
			</Card.Body>
		</Card>
	</div>
);
}

PortalCard.propTypes = {
course: PropTypes.shape({
name: PropTypes.string.isRequired,
description: PropTypes.string.isRequired,
price: PropTypes.number.isRequired,
isActive: PropTypes.bool.isRequired
})
}
