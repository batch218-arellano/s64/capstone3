import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Collapse } from 'react-bootstrap';
import FormExample from './FormExample';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

import {useParams, useNavigate, Link} from 'react-router-dom';


export default function CourseView() {

	

	const {user} = useContext(UserContext);
	

	const {courseId} = useParams(); 

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [isFormVisible, setIsFormVisible] = useState(false);

	const [cart, setCart] = useState([]);

	const navigate = useNavigate();

	const addToCart = (course) => {
		course.courseId = course._id
		fetch(`${process.env.REACT_APP_API_URL}/courses/${course._id}/setOrder`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(course)
		}).then(res => res.json())
		.then(data => {
			console.log(data);
			navigate("/checkout");
		})
		.catch(err => {
			console.log(err);
		});
	}


	useEffect(() => {

		console.log (courseId);


		fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
		

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})
	
	}, [courseId])

	return (
		<Container>
			<Row>
				<Col lg={{span: 6, offset:3}} >
					<Card>
						<Card.Body className="text-center">
							<Card.Title>{name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{description}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PhP {price}</Card.Text> 
							<Button variant="danger" 
							onClick={() => {
							addToCart({name, description, price, _id: courseId});
							Swal.fire({
								title: 'Sweet!',
								text: 'Your item has been added to your cart!',
								imageUrl: 'https://dogsbestlife.com/wp-content/uploads/2021/02/cats-vs.-dogs-group-scaled.jpeg',
								imageWidth: 400,
								imageHeight: 200,
								imageAlt: 'Custom image',
							}).then(() => {
								navigate("/courses");
							});
							}}> Add to cart</Button>  
							
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	) } 
