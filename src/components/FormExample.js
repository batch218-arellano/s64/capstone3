import React, { useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import editCourse from './editCourse';
import Swal from 'sweetalert2';

function FormExample({ name, description, price, courseId, history }) {
    const [formData, setFormData] = useState({
        name: name,
        description: description,
        price: price
    });
    const [isSuccessful, setIsSuccessful] = useState(false);
   
    useEffect(() => {
        if (isSuccessful) {
            history.push("/courses");
        }
    }, [isSuccessful, history]);

    const handleSubmit = (event) => {
        event.preventDefault();
        editCourse(courseId, formData)
            .then(response => {
                if (response.message === "Course updated successfully!") {
                    setIsSuccessful(true);
                } else {
                    Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                     });
                }
            })
            .catch(error => {
                console.log(error);
                 Swal.fire({
                     title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                 });
            });
    }

    return (
        <form onSubmit={handleSubmit}>
            <label>
                Name:
                <input type="text" name="name" value={formData.name} onChange={(e) => setFormData({ ...formData, name: e.target.value })} />
            </label>
            <br />
            <label>
                Description:
                <input type="text" name="description" value={formData.description} onChange={(e) => setFormData({ ...formData, description: e.target.value })} />
            </label>
            <br />
            <label>
                Price:
                <input type="number" name="price" value={formData.price} onChange={(e) => setFormData({ ...formData, price: e.target.value })} />
            </label>
            <br />
            <button type="submit">Submit</button>
        </form>
    )
}

export default FormExample
