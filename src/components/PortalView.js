import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Collapse } from 'react-bootstrap';
import FormExample from './FormExample';

import UserContext from '../UserContext';

import Swal from 'sweetalert2';

import {useParams, useNavigate, Link} from 'react-router-dom';


export default function CourseView() {

    const {user} = useContext(UserContext);
    

    const {courseId} = useParams(); 

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);

    const [isFormVisible, setIsFormVisible] = useState(false);

    const [cart, setCart] = useState([]);

    const navigate = useNavigate();

    const addToCart = (course) => {
        setCart([...cart, course]);
        navigate("/checkout")
    }

    const archive = (courseId) => {
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
            method: "PATCH",
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
            console.log(data)

            if(data.removed === true){
                Swal.fire({
                  title: "Successfully Removed",
                  icon: "success",
                  text: "You have successfully removed for this product."
                });
             }
             else {
                Swal.fire({
                  title: "Successfully Removed",
                  icon: "success",
                  text: "You have successfully removed for this product."
                });
             }

    })
};

const updateCourse = (courseId, newData) => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/update`, {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
        },
        body: JSON.stringify(newData)
    })
    .then(res => res.json())
    .then(data => {
        console.log(data)

        if(data.updated === true){
            Swal.fire({
              title: "Successfully Updated",
              icon: "success",
              text: "You have successfully updated this product."
            });
         }
         else {
            Swal.fire({
              title: "Successfully Updated",
              icon: "success",
              text: "You have successfully updated this product."
            });
         }

    });
};




    useEffect(() => {

        console.log (courseId);


        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
        

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);

        })
    
    }, [courseId])

    return (
        <Container>
            <Row>
                <Col lg={{span: 6, offset:3}} >
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>PhP {price}</Card.Text>
                            {user.id !== null 
                            ? 
                            <Button variant="danger" 
                            onClick={() => {
                            addToCart({name, description, price, _id: courseId});
                            Swal.fire({
                                title: 'Sweet!',
                                text: 'Your item has been added to your cart!',
                                imageUrl: 'https://dogsbestlife.com/wp-content/uploads/2021/02/cats-vs.-dogs-group-scaled.jpeg',
                                imageWidth: 400,
                                imageHeight: 200,
                                imageAlt: 'Custom image',
                            }).then(() => {
                                navigate("/courses");
                            });
                            }}>Add to cart</Button>

                            :
                            <>
                            <div className="d-flex justify-content-center">
                        <Button className="btn btn-danger mx-1" onClick={() => archive(courseId)} as={Link} to="/portal"  >Remove</Button>
                        <Button variant="warning" className="bg-warning mx-1" style={{display: user.id !== null ? 'none' : 'block'}} onClick={async () => {
                        const { value: text } = await Swal.fire({
                            input: 'textarea',
                            inputLabel: 'Your New Product',
                            inputPlaceholder: 'Type your new product here...',
                            inputAttributes: {
                            'aria-label': 'Type your message here'
                            },
                            showCancelButton: true
                        });
                        if (text) {
                            Swal.fire(text);
                        }
                        }} >Add Products</Button>

                        <Button variant="primary" onClick={() => setIsFormVisible(!isFormVisible)} className="mx-1">Edit</Button>
                        </div>

                            </>
                            }
                            <Collapse in={isFormVisible}>
                            <form onSubmit={(e) => {
                            e.preventDefault();
                            const newData = {
                                name: name,
                                description: description,
                                price: price
                            }
                            updateCourse(courseId, newData);
                            }}>
                            <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
                            <input type="text" value={description} onChange={(e) => setDescription(e.target.value)} />
                            <input type="number" value={price} onChange={(e) => setPrice(e.target.value)} />
                            <input type="submit" value="Submit" onClick={() => updateCourse(courseId, {name, description, price})} />
                            </form>
                            </Collapse>&nbsp;
                    
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}

