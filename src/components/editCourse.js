import React from 'react'
import Course from '../components/CourseCard';

function editCourse(courseId, reqBody) {
    return Course.findByIdAndUpdate(courseId, {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }, { new: true }).then((updatedCourse) => {
        return updatedCourse;
    });
}

export default editCourse
