import {Row, Col , Card} from 'react-bootstrap';

export default function Highlights() {
	return (
	    <Row className="mt-3 mb-3">
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Shop anytime, anywhere!</h2>
	                    </Card.Title>
	                    <Card.Text>
						Experience the convenience of shopping from the comfort of your own home with our online store. Shop anytime, anywhere with just a few clicks. Whether you're on the go or lounging at home, our website makes it easy to find the products you need and have them delivered straight to your door. Say goodbye to the hassle of going to a physical store and hello to easy, stress-free shopping with us.
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>We accept any forms of payment!</h2>
	                    </Card.Title>
	                    <Card.Text>
						Say goodbye to the hassle of limited payment options. At Kibble Co., we accept any form of payment, making your shopping experience more convenient and stress-free. Whether you prefer to pay with cash, credit, debit, or even digital wallets, we've got you covered. Shop with ease knowing that we accept any forms of payment. Shop now!
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	        <Col xs={12} md={4}>
	            <Card className="cardHighlight p-3">
	                <Card.Body>
	                    <Card.Title>
	                        <h2>Partner with us and become a seller!</h2>
	                    </Card.Title>
	                    <Card.Text>
						Looking to expand your business and reach a wider audience? Partner with us and become a seller! Our platform offers a convenient and easy way for you to sell your products to customers all over the world. With our flexible and secure payment options, you can focus on growing your business and leave the rest to us. Sign up now and start reaching new customers today!
	                    </Card.Text>
	                </Card.Body>
	            </Card>
	        </Col>
	    </Row>
	)
}