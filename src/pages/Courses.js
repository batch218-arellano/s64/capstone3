import CourseCard from '../components/CourseCard';
import { useState, useEffect } from 'react';

export default function Courses() {
  const [activeCourses, setActiveCourses] = useState([]);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
      .then(res => res.json())
      .then(data => {
        setActiveCourses(data.map(course => {
          return <CourseCard key={course._id} course={course} />;
        }));
      });
  }, []);

  return (
    <div>
      <h1>Products for pets</h1>
      <div>{activeCourses}</div>
    </div>
  );
}
// useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/courses/inactive`)
//     .then(res => res.json())
//     .then(data => {
//         console.log(data)
//         setInactiveCourses(data.map(course => {
//             return (
//                 <CourseCard key={course._id} course={course} />
//             )
//         }))
//     })
// }, [])

// }

// return(
//     <>
//         <h2>Active Products</h2>
//         {activeCourses}
//         <h2>Inactive Products</h2>
//         {inactiveCourses}
//     </>
// )}
