import Banner from '../components/Banner';
import Highlights from '../components/Highlights'; 

export default function Home() {

	const data = {

		title: "Kibble Co.",
		content: "A shop for pet lovers",
		destination: "/register",
		label: "Get Started!"

	}
	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
};

