import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'; 

export default function Login() {
const { user, setUser } = useContext(UserContext);
const [email, setEmail] = useState('');
const [password, setPassword] = useState('');
const [isActive, setIsActive] = useState(true);
const [isLoggedIn, setIsLoggedIn] = useState(false);

function authenticate(e) {
    e.preventDefault();
    if (email === 'adminadmin@mail.com' && password === 'admin') {
        setIsLoggedIn(true);
        setUser({ id: 1, email: email }); 
        Swal.fire({
            title: 'Success!',
            text: 'You are now logged in',
            icon: 'success',
            timer: 2000,
            showConfirmButton: false,
        });
        setTimeout(() => {
            window.location.href = '/portal'
        }, 2000)
    } else {
        Swal.fire({
            title: 'Error!',
            text: 'Invalid email or password',
            icon: 'error',
            timer: 2000,
            showConfirmButton: false,
        });
        setTimeout(() => {
            window.location.href = '/courses'
        }, 2000)
    }
    setEmail('');
    setPassword('');
};

return (
<Form onSubmit={(e) => authenticate(e)}>
<Form.Group controlId="userEmail">
<Form.Label>Email address</Form.Label>
<Form.Control
type="email"
placeholder="Enter email"
value={email}
onChange={(e) => setEmail(e.target.value)}
required
/>
</Form.Group>


        <Form.Group controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
            type="password"
            placeholder="Password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
        />
    </Form.Group>
    <Button variant="primary" type="submit" disabled={!isActive}>
        Submit
    </Button>
</Form>
);
}